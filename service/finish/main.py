#!/usr/bin/env python

import asyncio
import aioredis
import logging
import json
import random

redis = None
pub = None
service_id = None
redis_address = 'redis://p3r_redis'
redis_stream = 'tasks_stream'
redis_channel = 'tasks_response'
state_in = 'task_registered'
state_out = 'task_finished'

async def redis_connect():
    global redis, pub
    redis = await aioredis.create_redis(redis_address)
    pub = await aioredis.create_redis(redis_address)

async def task_process(data):
  logging.debug(data)
  resp = {}
  try:
      resp['req_id'] = data['req_id']
      resp['nama'] = data['name']
      resp['nomor'] = data['number']
      resp['service_id'] = service_id
      resp['state'] = state_out
      resp['sleep'] = random.uniform(0.1,1.9)
      await asyncio.sleep(resp['sleep'])

  except Exception:
    logging.debug('error decode msg')
  finally:
    return resp

async def main():
    await redis_connect()
    latestId = 0
    msgb = {}
    msg = {}
    
    while True:
        data = await redis.xread([redis_stream], count=1, latest_ids=[latestId], timeout=3000)
        if (len(data) > 0):
          latestId = data[0][1]
          msgb.update(data[0][2])
          for k,v in msgb.items():
            msg[k.decode('utf-8')] = v.decode('utf-8')
          
          if (msg['state'] == state_in):
            resp = await task_process(msg)
            await pub.publish(redis_channel, json.dumps(resp))
            await redis.xdel(redis_stream, latestId)
        


if __name__ == '__main__':
  service_id = random.randint(10,99)
  logging.basicConfig(level=logging.DEBUG)
  logging.debug(service_id)
  asyncio.run(main())
