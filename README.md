# demo_python_redis

## Setup

### Docker images

```
$ docker build -t python-asyncio:alpine ./python-asynchio/
...

$ docker build -t komentr/p3rserver ./server/
Sending build context to Docker daemon  6.144kB
Step 1/5 : FROM python-asyncio:alpine
 ---> 2097f0207b7a
Step 2/5 : WORKDIR /app
 ---> Using cache
 ---> 40d36a6e295a
Step 3/5 : ADD . /app
 ---> d6e0b919c539
Step 4/5 : EXPOSE 8080
 ---> Running in e88441fcabdd
Removing intermediate container e88441fcabdd
 ---> 01463d2f8d9f
Step 5/5 : CMD ["python", "main.py"]
 ---> Running in 9ba61dc0d5db
Removing intermediate container 9ba61dc0d5db
 ---> 7f185c149f9d
Successfully built 7f185c149f9d
Successfully tagged komentr/p3rserver:latest

$ docker build -t komentr/p3worker ./worker/
Sending build context to Docker daemon   5.12kB
Step 1/5 : FROM python-asyncio:alpine
 ---> 2097f0207b7a
Step 2/5 : WORKDIR /app
 ---> Running in e8112cbac3b9
Removing intermediate container e8112cbac3b9
 ---> 40d36a6e295a
Step 3/5 : ADD . /app
 ---> de5304b5ad80
Step 4/5 : EXPOSE 8080
 ---> Running in 031838a6f748
Removing intermediate container 031838a6f748
 ---> 273e4a97a61e
Step 5/5 : CMD ["python", "main.py"]
 ---> Running in 87221c8487db
Removing intermediate container 87221c8487db
 ---> 3d3d9fd87dfd
Successfully built 3d3d9fd87dfd
Successfully tagged komentr/p3rworker:latest
```

## 

Run redis server:
```
$ docker run -d --name=p3r_redis -p "6379:6379" -v "/var/docker/p3redis/redis:/data" --network switch redis:latest
```

Run server manually:
```
$ cd server
$ docker run --rm -p "8080:8080" -v "$PWD":/app -w /app --network switch python-asyncio:alpine python main.py
```

Run worker manually:
```
$ cd worker
$ docker run --rm -v "$PWD":/app -w /app --network switch python-asyncio:alpine python main.py
```


## Docker-compose

```
$ docker-compose up --scale worker=3
Starting p3r_redis ... done
Starting p3r_server ... done
Starting demo_python_redis_worker_1 ... done
Starting demo_python_redis_worker_2 ... done
Starting demo_python_redis_worker_3 ... done
Attaching to p3r_redis, p3r_server, demo_python_redis_worker_1, demo_python_redis_worker_2, demo_python_redis_worker_3
worker_1  | DEBUG:root:33
worker_1  | DEBUG:asyncio:Using selector: EpollSelector
p3r_redis | 1:C 13 Sep 2020 15:06:35.571 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
p3r_redis | 1:C 13 Sep 2020 15:06:35.571 # Redis version=6.0.6, bits=64, commit=00000000, modified=0, pid=1, just started
p3r_redis | 1:C 13 Sep 2020 15:06:35.571 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
p3r_server | DEBUG:asyncio:Using selector: EpollSelector
p3r_server | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * Running mode=standalone, port=6379.
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 # Server initialized
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
worker_1  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_1  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
worker_1  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_1  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * Loading RDB produced by version 6.0.6
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * RDB age 180 seconds
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * RDB memory usage when created 0.76 Mb
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * DB loaded from disk: 0.000 seconds
p3r_redis | 1:M 13 Sep 2020 15:06:35.572 * Ready to accept connections
p3r_server | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
p3r_server | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
worker_3  | DEBUG:root:10
worker_3  | DEBUG:asyncio:Using selector: EpollSelector
worker_3  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_3  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
worker_3  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_3  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
worker_2  | DEBUG:root:59
worker_2  | DEBUG:asyncio:Using selector: EpollSelector
worker_2  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_2  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
worker_2  | DEBUG:aioredis:Parsed Redis URI ('p3r_redis', 6379)
worker_2  | DEBUG:aioredis:Creating tcp connection to ('p3r_redis', 6379)
```

## Logs

### Client tester logs

```
$ curl -X GET http://localhost:8080/list/asep/6
{"req_id": "940931", "nama": "asep", "nomor": "6", "worker_id": 33, "sleep": 0.7351418300410567}
$ curl -X GET http://localhost:8080/list/asep/10
{"req_id": "606834", "nama": "asep", "nomor": "10", "worker_id": 10, "sleep": 0.30661748544962636}
$ curl -X GET http://localhost:8080/list/asep/111
{"req_id": "530962", "nama": "asep", "nomor": "111", "worker_id": 59, "sleep": 1.1049385561161125}
$ curl -X GET http://localhost:8080/list/asep/11
{"req_id": "813186", "nama": "asep", "nomor": "11", "worker_id": 33, "sleep": 1.8278455394749296}
```

### Server logs: response to client tester

```
worker_1  | DEBUG:root:[b'tasks_list', b'{"req_id": "940931", "name": "asep", "number": "6"}']
p3r_server | DEBUG:root:{'req_id': '940931', 'nama': 'asep', 'nomor': '6', 'worker_id': 33, 'sleep': 0.7351418300410567}
p3r_server | INFO:aiohttp.access:172.19.0.1 [13/Sep/2020:15:07:18 +0000] "GET /list/asep/6 HTTP/1.1" 200 253 "-" "curl/7.68.0"
worker_3  | DEBUG:root:[b'tasks_list', b'{"req_id": "606834", "name": "asep", "number": "10"}']
p3r_server | DEBUG:root:{'req_id': '606834', 'nama': 'asep', 'nomor': '10', 'worker_id': 10, 'sleep': 0.30661748544962636}
p3r_server | INFO:aiohttp.access:172.19.0.1 [13/Sep/2020:15:07:21 +0000] "GET /list/asep/10 HTTP/1.1" 200 255 "-" "curl/7.68.0"
worker_2  | DEBUG:root:[b'tasks_list', b'{"req_id": "530962", "name": "asep", "number": "111"}']
p3r_server | DEBUG:root:{'req_id': '530962', 'nama': 'asep', 'nomor': '111', 'worker_id': 59, 'sleep': 1.1049385561161125}
p3r_server | INFO:aiohttp.access:172.19.0.1 [13/Sep/2020:15:07:23 +0000] "GET /list/asep/111 HTTP/1.1" 200 255 "-" "curl/7.68.0"
worker_1  | DEBUG:root:[b'tasks_list', b'{"req_id": "813186", "name": "asep", "number": "11"}']
p3r_server | DEBUG:root:{'req_id': '813186', 'nama': 'asep', 'nomor': '11', 'worker_id': 33, 'sleep': 1.8278455394749296}
p3r_server | INFO:aiohttp.access:172.19.0.1 [13/Sep/2020:15:07:31 +0000] "GET /list/asep/11 HTTP/1.1" 200 254 "-" "curl/7.68.0"
```

## Stop

```
^CGracefully stopping... (press Ctrl+C again to force)
Stopping demo_python_redis_worker_3 ... done
Stopping demo_python_redis_worker_2 ... done
Stopping demo_python_redis_worker_1 ... done
Stopping p3r_server                 ... done
Stopping p3r_redis                  ... done
```