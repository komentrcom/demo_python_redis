#!/usr/bin/env python

import asyncio
import aioredis
import logging
import json
import random

redis = None
pub = None
worker_id = None
redis_address = 'redis://p3r_redis'
redis_list = 'tasks_list'
redis_channel = 'tasks_response'


async def redis_connect():
    global redis, pub
    redis = await aioredis.create_redis(redis_address)
    pub = await aioredis.create_redis(redis_address)

async def task_process(msg):
  resp = {}
  try:
    logging.debug(msg)
    data = json.loads(msg[1])
    resp['req_id'] = data['req_id']
    resp['nama'] = data['name']
    resp['nomor'] = data['number']
    resp['worker_id'] = worker_id
    resp['sleep'] = random.uniform(0.1,1.9)
    await asyncio.sleep(resp['sleep'])
  except Exception:
    logging.debug('error decode msg')
  finally:
    return resp

async def main():
    await redis_connect()
    
    while True:
        msg = await redis.brpop(redis_list, timeout=0)
        resp = await task_process(msg)
        await pub.publish(redis_channel, json.dumps(resp))


if __name__ == '__main__':
  worker_id = random.randint(10,99)
  logging.basicConfig(level=logging.DEBUG)
  logging.debug(worker_id)
  asyncio.run(main())
