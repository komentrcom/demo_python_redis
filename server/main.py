# server_simple.py
from aiohttp import web
import aioredis
import asyncio
import logging
import json
from datetime import datetime

redis_pool = None
redis_sub = None
redis_sub_channel = None
redis_address = 'redis://p3r_redis'
redis_channel = 'tasks_response'
redis_list = 'tasks_list'
redis_stream = 'tasks_stream'

async def redis_create(app):
  try:
    app['redis'] = await aioredis.create_redis((redis_address))
    ch = await app['redis'].subscribe(redis_channel)
    app['redis_channel'] = ch[0]

  except asyncio.CancelledError:
      logging.debug(asyncio.CancelledError)
  # finally:
  #     logging.debug('redis_create failed')

async def redis_close(app):
  await app['redis'].unsubscribe(app['redis_channel'].name)
  app['redis'].close()
  await app['redis'].wait_closed()

async def redis_pool_create(app):
  try:
    app['redis_pool'] = await aioredis.create_redis_pool(
          redis_address,
          minsize=1, maxsize=10
        )
  except asyncio.CancelledError:
      logging.debug(asyncio.CancelledError)
  # finally:
  #     logging.debug('redis_pool_create failed')

async def redis_pool_close(app):
  app['redis_pool'].close()
  await app['redis_pool'].wait_closed()

async def channel_reader(ch, key):
  # while True:
    while (await ch.wait_message()):
      try:
        msg = await ch.get_json()
        logging.debug(msg)
        if (msg['req_id'] == key):
          return msg
      except Exception:
        logging.debug('error decode msg')

async def stream_task(redis_pool, msg):
    with (await redis_pool) as conn:
      await conn.xadd(redis_stream, msg)

async def streamhandle(request):
    resp = None
    msg = {}
    req_id = datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3]
    name = request.match_info.get("name", "Anonymous")
    number = request.match_info.get("number", "0")
    msg['req_id'] = req_id
    msg['name'] = name
    msg['number'] = number
    msg['state'] = 'task_registered'

    await stream_task(request.app['redis_pool'], msg)

    coro = asyncio.create_task(channel_reader(request.app['redis_channel'], req_id))
    done, pending = await asyncio.wait({coro}, timeout=15, return_when=asyncio.FIRST_COMPLETED)
    for task in done:
      resp = task.result()
    for task in pending:
      task.cancel()
    # return web.Response(text=text)
    return web.json_response(resp)

async def list_task(redis_pool, msg):
    with (await redis_pool) as conn:
        await conn.lpush(redis_list, msg)

async def listhandle(request):
    resp = None
    msg = {}
    req_id = datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3]
    name = request.match_info.get("name", "Anonymous")
    number = request.match_info.get("number", "0")
    msg['req_id'] = req_id
    msg['name'] = name
    msg['number'] = number

    await list_task(request.app['redis_pool'], json.dumps(msg))

    coro = asyncio.create_task(channel_reader(request.app['redis_channel'], req_id))
    done, pending = await asyncio.wait({coro}, timeout=15, return_when=asyncio.FIRST_COMPLETED)
    for task in done:
      resp = task.result()
    for task in pending:
      task.cancel()
    # return web.Response(text=text)
    return web.json_response(resp)

def init():
  app = web.Application()
  logging.basicConfig(level=logging.DEBUG)
  app.router.add_get("/list/{name}/{number}", listhandle)
  app.router.add_get("/stream/{name}/{number}", streamhandle)
  app.on_startup.append(redis_pool_create)
  app.on_startup.append(redis_create)
  app.on_cleanup.append(redis_pool_close)
  app.on_cleanup.append(redis_close)
  
  return app

web.run_app(init())
